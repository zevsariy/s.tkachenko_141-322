from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.cache import cache_page


from .models import Kontacts

def uuu(request):
 kontacts_list = Kontacts.objects.all()
 paginator = Paginator(kontacts_list, 1)
 page = request.GET.get('page')
 try:
     kontacts = paginator.page(page)
 except PageNotAnInteger:
     kontacts = paginator.page(1)
 except EmptyPage:
     kontacts = paginator.page(paginator.num_pages)
 context = {'kontacts': kontacts}
 return render_to_response('kontacts/list.html', context ,context_instance=RequestContext(request))

@cache_page(60 * 15)
def index(request):
 kontacts_list = Kontacts.objects.all()
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/index.html', context)
 
def filter_name1(request):
 kontacts_list = Kontacts.objects.filter(dolznost='Worker')
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/filter.html', context)

def filter_name2(request):
 kontacts_list = Kontacts.objects.filter(dolznost='Director')
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/filter.html', context)
 
def filter_name3(request):
 kontacts_list = Kontacts.objects.filter(dolznost='Proger')
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/filter.html', context)
 
def filter_name4(request):
 kontacts_list = Kontacts.objects.filter(dolznost='Washer')
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/filter.html', context)
 
def filter_name5(request):
 kontacts_list = Kontacts.objects.filter(dolznost='IT')
 context = {'kontacts': kontacts_list}
 return render(request, 'kontacts/filter.html', context)
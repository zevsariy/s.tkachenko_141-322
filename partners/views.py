from django.shortcuts import render, render_to_response, redirect
from . forms import PartForm, PartFormAdd
from django.template import RequestContext


from .models import partners
def index(request):
 partners_list = partners.objects.all()
 context = {'partners': partners_list}
 return render(request, 'partners/index.html', context)
 
def change2(request, p_id ):
 p = partners.objects.get(id = p_id)
 print p,"  ", p.id
 p.bool=True
 p.save()
 partners_list = partners.objects.all()
 context = {'partners': partners_list}
 return render(request, 'partners/index.html', context)
 
def change(request, p_id):
    partner = partners.objects.get(id = p_id)
    if request.method=='POST' :
        form = PartForm(request.POST, instance=partner)
        if form.is_valid():
            form.save()
            partners_list = partners.objects.all()
            context = {'partners': partners_list}
            return render(request, 'partners/index.html', context)
    else:
        form = PartForm(instance=partner)
    context = {'form': form}
    return render(request, 'partners/change.html', context)
    

def add(request):
    if request.method=='POST' :
        print(request.FILES)
        form = PartFormAdd(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            partners_list = partners.objects.all()
            context = {'partners': partners_list}
            return render(request, 'partners/index.html', context)
    else:
        form = PartFormAdd()
    context = {'form': form}
    return render(request, 'partners/change.html', context)
    
    
def delete(request, p_id):
    partner_del = partners.objects.get(id = p_id)
    partner_del.delete()
    partners_list = partners.objects.all()
    context = {'partners': partners_list}
    return redirect('partners/index.html')
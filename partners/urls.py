from django.contrib import admin
from django.conf.urls import patterns, url
from partners import views
urlpatterns = patterns('',
 url(r'^$', views.index, name='index'),
 url(r'^add/', views.add, name='add'),
 url(r'^delete/(?P<p_id>\d)/$', views.delete, name='delete'),
 url(r'^(?P<p_id>\d)/$', views.change, name='change'),)
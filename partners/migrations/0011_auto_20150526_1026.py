# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0010_partners_bool'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='bool',
            field=models.BooleanField(default=True, verbose_name=b'Publicate'),
        ),
    ]

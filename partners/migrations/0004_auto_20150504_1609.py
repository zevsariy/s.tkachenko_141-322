# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0003_partners_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='partners',
            old_name='name',
            new_name='description',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='dolznost',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='email',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='phone',
        ),
    ]

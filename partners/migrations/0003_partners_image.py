# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0002_auto_20150504_1407'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='image',
            field=models.ImageField(default=111, upload_to=b''),
            preserve_default=False,
        ),
    ]

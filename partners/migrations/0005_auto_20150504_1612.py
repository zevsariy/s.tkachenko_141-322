# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0004_auto_20150504_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='image',
            field=models.ImageField(upload_to=b'templates/images/p/'),
        ),
    ]

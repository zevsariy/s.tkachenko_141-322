# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0009_auto_20150505_2315'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='bool',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]

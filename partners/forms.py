# coding: utf-8

from django.forms import ModelForm
from . models import partners

class PartForm(ModelForm):
    class Meta:
        model = partners
        fields = ['description', 'bool']
        
class PartFormAdd(ModelForm):
    class Meta:
        model = partners
        fields = ['image', 'description', 'bool']
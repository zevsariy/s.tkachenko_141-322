from django.contrib import admin

from .models import partners

class setad(admin.ModelAdmin):
    list_display = ('image', 'description', 'bool')
    search_fields = ['description']


admin.site.register(partners, setad)

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from vistuplenia.forms import VistFrom
from vistuplenia.models import vistuplenia
from django.template import RequestContext
from django.http import HttpResponseRedirect

from .models import vistuplenia

def index(request):
    vistuplenia_list = vistuplenia.objects.all()
    context = {'vistuplenia': vistuplenia_list}
    return render(request, 'vistuplenia/index.html', context)
    
def done(request):
    vistuplenia_list = vistuplenia.objects.all()
    context = {'vistuplenia': vistuplenia_list}
    return render(request, 'vistuplenia/done.html', context)

def add(request, template_name='vistuplenia/add.html'):
    if request.method == 'POST':
        postdata = request.POST.copy()
        form = VistFrom(request, postdata)
        if form.is_valid():
            name = '%s %s' % (postdata['first_name'], postdata['last_name'])
            ns = vistuplenia(speaker=name, tema=postdata['topic'], date=postdata['data'])
            ns.save()
            return redirect('../done')
    else:
        form = VistFrom()
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))
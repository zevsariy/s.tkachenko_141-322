# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vistuplenia', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='News',
            new_name='vistuplenia',
        ),
        migrations.RenameField(
            model_name='vistuplenia',
            old_name='pub_date',
            new_name='date',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vistuplenia', '0003_auto_20150504_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vistuplenia',
            name='date',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='vistuplenia',
            name='speaker',
            field=models.CharField(max_length=255),
        ),
    ]

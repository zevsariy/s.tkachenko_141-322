# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vistuplenia', '0002_auto_20150504_1407'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vistuplenia',
            old_name='title',
            new_name='speaker',
        ),
        migrations.RenameField(
            model_name='vistuplenia',
            old_name='description',
            new_name='tema',
        ),
        migrations.RemoveField(
            model_name='vistuplenia',
            name='text',
        ),
    ]

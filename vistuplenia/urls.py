from django.contrib import admin
from django.conf.urls import patterns, url
from vistuplenia import views
urlpatterns = patterns('',
 url(r'^$', views.index, name='index'),
 url(r'^add/', views.add, name='add'),
  url(r'^done/', views.done, name='done'),)


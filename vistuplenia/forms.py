# coding: utf-8

from django import forms

class VistFrom(forms.Form):
    first_name = forms.CharField(label='Имя', max_length=100)
    last_name = forms.CharField(label='Фамилия', max_length=100)
    topic = forms.CharField(label='Тема выступления', max_length=500)
    data = forms.CharField(label='Дата выступления', max_length=500)

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super(VistFrom, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data
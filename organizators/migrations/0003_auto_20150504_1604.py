# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0002_auto_20150504_1407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organizators',
            name='email',
        ),
        migrations.RemoveField(
            model_name='organizators',
            name='phone',
        ),
        migrations.AddField(
            model_name='organizators',
            name='rol',
            field=models.CharField(default=1111, max_length=150),
            preserve_default=False,
        ),
    ]

from django.shortcuts import render

from .models import organizators
def index(request):
 organizators_list = organizators.objects.all()
 context = {'organizators': organizators_list}
 return render(request, 'organizators/index.html', context)

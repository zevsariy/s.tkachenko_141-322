# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0002_auto_20150504_1407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='speakers',
            name='email',
        ),
        migrations.RemoveField(
            model_name='speakers',
            name='phone',
        ),
    ]

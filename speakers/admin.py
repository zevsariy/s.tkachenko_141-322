from django.contrib import admin

from .models import speakers

class setsp(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ['name']


    
admin.site.register(speakers, setsp)


from django.shortcuts import render

from .models import speakers
def index(request):
 speakers_list = speakers.objects.all()
 context = {'speakers': speakers_list}
 return render(request, 'speakers/index.html', context)